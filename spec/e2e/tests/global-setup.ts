import { test as setup } from '@playwright/test';
import { v2 as compose } from "docker-compose";
import { env } from "process";
import { readFileSync } from "fs";
import yaml from "js-yaml";

const composeProjectName = env.COMPOSE_PROJECT_NAME || "dependabot";
const mockImage = env.MOCK_IMAGE || "thiht/smocker:0.18.2";
const imageTag = env.CURRENT_TAG || "latest";
const imageName = env.APP_IMAGE_NAME;
const baseImage = imageName ? `${imageName}:${imageTag}` : undefined;

const composeYml = () => {
  const yml = yaml.load(readFileSync("docker-compose.yml") as any) as any;
  yml["services"]["gitlab"] = {
    image: mockImage,
    ports: ["8081:8081"]
  };

  return yaml.dump(yml, {
    styles: {
      "!!null": "empty"
    }
  });
};

const composeEnv = {
  BASE_IMAGE: baseImage,
  UPDATER_IMAGE_PATTERN: imageName ? `${imageName}-%<package_ecosystem>s:${imageTag}` : undefined,
  SETTINGS__GITLAB_URL: "http://gitlab:8080",
  SETTINGS__DEPENDABOT_URL: "http://dependabot-gitlab.com",
  SETTINGS__GITLAB_ACCESS_TOKEN: "e2e-test",
  SETTINGS__GITHUB_ACCESS_TOKEN: env.GITHUB_ACCESS_TOKEN_TEST,
  SETTINGS__LOG_COLOR: "true",
  REDIS_EXTRA_FLAGS: "--protected-mode no"
};

const composeOpts: compose.IDockerComposeOptions = {
  env: { ...env, ...composeEnv },
  composeOptions: ["--project-name", composeProjectName],
  log: false
};

const setupE2eEnvironment = async () => {
  const yml = composeYml();

  console.log("*** Setting up E2E environment ***");
  console.log("Pulling images...");
  await compose.pullAll({ ...composeOpts, configAsString: yml, commandOptions: ["--include-deps", "-q"] });
  console.log("  successfully pulled images");

  console.log("Starting services...");
  await compose.upAll({ ...composeOpts, configAsString: yml, commandOptions: ["--wait"] });
  console.log("  successfully started test environment");
};

setup("set-up environment", async () => {
  if (env.SETUP_E2E_ENVIRONMENT != "true") setup.skip();
  setup.slow();

  await setupE2eEnvironment();
});
