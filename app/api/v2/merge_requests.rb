# frozen_string_literal: true

module V2
  class MergeRequests < Grape::API
    include AuthenticationSetup
    include Pagination

    helpers do
      params :merge_request_params do
        optional :state, type: String, desc: "Filter by state"
        optional :package_ecosystem, type: String, desc: "Filter by package ecosystem"
        optional :main_dependency, type: String, desc: "Filter by main dependency"
      end

      def mr_filter_args
        {
          state: params[:state],
          package_ecosystem: params[:package_ecosystem],
          main_dependency: params[:main_dependency]
        }.compact
      end
    end

    namespace :merge_requests do
      desc "Get all merge requests" do
        detail "Return array of all merge requests"
        is_array true
        success model: MergeRequest::Entity,
                examples: [MergeRequest::Entity.example_response],
                message: "Successful response"
      end
      params do
        use :pagination_params
        use :merge_request_params
      end
      get do
        present paginate(MergeRequest.where(**mr_filter_args))
      end

      route_param :id, type: String, desc: "Merge request id" do
        desc "Get single merge request" do
          detail "Return single merge request"
          success model: MergeRequest::Entity,
                  examples: MergeRequest::Entity.example_response,
                  message: "Successful response"
        end
        get do
          present MergeRequest.find_by(id: params[:id])
        end
      end
    end

    namespace :projects do
      helpers APIHelpers

      route_param :id, type: String, desc: "Project id or url encoded full path" do
        namespace :merge_requests do
          desc "Get all merge requests" do
            detail "Return array of merge requests for a project"
            is_array true
            success model: MergeRequest::Entity,
                    examples: [MergeRequest::Entity.example_response],
                    message: "Successful response"
          end
          params do
            use :pagination_params
            use :merge_request_params
          end
          get do
            present paginate(project.merge_requests.where(**mr_filter_args))
          end
        end
      end
    end
  end
end
