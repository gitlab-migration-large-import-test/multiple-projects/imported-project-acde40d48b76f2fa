# frozen_string_literal: true

module Dependabot
  module Projects
    module Registration
      class JobCreator < ApplicationService
        JOB_NAME = "Project Registration"
        JOB_CLASS = ProjectRegistrationJob

        # Create or destroy project registration job
        #
        # @return [void]
        def call
          if mode == "automatic"
            create unless job_synced?
            schedule if AppConfig.project_registration_run_on_boot?
          elsif %w[manual system_hook].include?(mode) && job
            destroy
          end
        end

        private

        # Create project registration job
        #
        # @return [void]
        def create
          log(:info, "Creating project registration job")
          Sidekiq::Cron::Job.create(job_args)
        end

        # Schedule job to run immediatelly
        #
        # @return [void]
        def schedule
          ProjectRegistrationJob.perform_later
        end

        # Remove project registration job
        #
        # @return [void]
        def destroy
          log(:info, "Removing project registration job")
          Sidekiq::Cron::Job.destroy(JOB_NAME)
        end

        # Job with synced cron
        #
        # @return [Boolean]
        def job_synced?
          job&.to_hash&.values_at(:name, :cron, :klass) == job_args.values_at(:name, :cron, :class) &&
            job&.queue_name_with_prefix == job_args[:queue]
        end

        # Existing job
        #
        # @return [Sidekiq::Cron::Job]
        def job
          @job ||= Sidekiq::Cron::Job.find(JOB_NAME)
        end

        # Job arguments
        #
        # @return [Hash]
        def job_args
          @job_args ||= {
            name: JOB_NAME,
            cron: cron,
            class: JOB_CLASS.name,
            active_job: true,
            queue: JOB_CLASS.queue_name,
            description: "Automatically register projects for update"
          }
        end

        # Job cron
        #
        # @return [String]
        def cron
          @cron ||= AppConfig.project_registration_cron
        end

        # Project registration mode
        #
        # @return [String]
        def mode
          @mode ||= AppConfig.project_registration
        end
      end
    end
  end
end
