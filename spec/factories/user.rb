# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    username { Faker::Alphanumeric.alpha(number: 10) }
    password { Faker::Internet.password }
  end
end
