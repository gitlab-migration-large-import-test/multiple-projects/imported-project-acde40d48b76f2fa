# frozen_string_literal: true

Sidekiq.configure_server do |config|
  # Configure sidekiq health check
  require "sidekiq_alive"

  SidekiqAlive::Worker.sidekiq_options log_level: Logger::Severity::WARN
  SidekiqAlive.setup do |alive_config|
    alive_config.path = "/healthcheck"
    alive_config.custom_liveness_probe = proc { Mongoid.default_client.database_names.present? }
    alive_config.time_to_live = AppConfig.sidekiq_alive_key_ttl
    alive_config.queue_prefix = :healthcheck
  end

  # Configure sidekiq server
  config.logger = Rails.logger
  config.redis = AppConfig.redis_config

  # Remove alive queue on shutdown
  config.on(:shutdown) do
    Sidekiq::Queue.all.find { |q| q.name == SidekiqAlive.current_queue }&.clear
  end
end

Sidekiq.configure_client do |config|
  config.logger = Rails.logger
  config.redis = AppConfig.redis_config.merge(size: ((ENV["RAILS_MAX_THREADS"] || 5).to_f / 2).round)
end

# Reduce verbose output of activejob
ActiveJob::Base.logger = Logger.new(IO::NULL)
