# frozen_string_literal: true

require_relative "../task_helpers/full_container_scan"

# rubocop:disable Rails/RakeEnvironment
namespace :ci do
  desc "Merge coverage report from multiple files"
  task :merge_coverage do
    require "simplecov"

    SimpleCov.collate Dir["reports/coverage/*/.resultset.json"]
  end

  desc "Run full snyk container vulnerability scan"
  task :container_scan, %i[app_image check_app_vulnerabilities] => :environment do |_, args|
    scan =  FullContainerScan.new(args[:app_image], check_app_vulnerabilities: args[:check_app_vulnerabilities]).run

    exit(1) unless scan.success?
  end
end
# rubocop:enable Rails/RakeEnvironment
